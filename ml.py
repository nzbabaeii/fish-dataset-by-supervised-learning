import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn import metrics
from sklearn.preprocessing import OneHotEncoder, MinMaxScaler, PolynomialFeatures
from sklearn.model_selection import train_test_split, GridSearchCV, learning_curve, cross_val_score
from sklearn.linear_model import LinearRegression, LogisticRegression, Ridge, Lasso
from sklearn.metrics import accuracy_score, confusion_matrix, classification_report
from sklearn.naive_bayes import GaussianNB
from sklearn.neighbors import KNeighborsClassifier



def load_data(file_path):
    return pd.read_csv(file_path)

def preprocess_data(df, selected_features):
    categorical_features = ['Species']
    numerical_features = [col for col in selected_features if col != 'Species']
    
    encoder = OneHotEncoder(drop='first')
    encoder.fit(df[categorical_features])
    encoded_categorical = encoder.transform(df[categorical_features]).toarray()
    encoded_categorical_df = pd.DataFrame(encoded_categorical, columns=encoder.get_feature_names_out(categorical_features))
    
    df_combined = pd.concat([df[numerical_features], encoded_categorical_df], axis=1)
    
    # Normalizing the data
    cont_attr = df_combined.columns.tolist()
    values_normalized = df_combined.values
    scaler = MinMaxScaler().fit_transform(values_normalized)
    df_combined = pd.DataFrame(scaler, columns=cont_attr)
    
    return df_combined

def train_mlr(X_train, y_train):
    mlr = LinearRegression()
    mlr.fit(X_train, y_train)
    return mlr

def evaluate_model(model, X_test, y_test):
    y_pred = model.predict(X_test)
    r2 = metrics.r2_score(y_test, y_pred)
    mae = metrics.mean_absolute_error(y_test, y_pred)
    mse = metrics.mean_squared_error(y_test, y_pred)
    rmse = np.sqrt(mse)
    return r2, mae, mse, rmse, y_pred

def plot_actual_vs_predicted(y_test, y_pred):
    plt.figure(figsize=(10, 5))
    plt.scatter(y_test, y_pred)
    plt.xlabel("Actual Weight")
    plt.ylabel("Predicted Weight")
    plt.title("Actual vs Predicted Weight")
    plt.minorticks_on()
    plt.grid(which='both', linestyle='--', linewidth=0.5)
    return plt

def train_polynomial_regression(X_train, y_train, degree):
    poly_features = PolynomialFeatures(degree=degree)
    X_train_poly = poly_features.fit_transform(X_train)
    model = LinearRegression()
    model.fit(X_train_poly, y_train)
    return model, poly_features

def plot_residuals(y_test, y_pred):
    plt.figure()
    sns.histplot(y_test - y_pred, kde=True)
    plt.title("Histogram of Residuals")
    plt.xlabel("Residuals")
    plt.ylabel("Frequency")
    plt.minorticks_on()
    plt.grid(which='both', linestyle='--', linewidth=0.5)
    return plt

def correlation_matrix(df):
    return df.corr()

def split_data(df_combined, target='Weight'):
    Y = np.array(df_combined[target])
    X = np.array(df_combined.drop(columns=[target]))
    return train_test_split(X, Y, test_size=0.2, random_state=4)

def preprocess_classification_data(df):
    df_grouped = df.copy()
    df_grouped['Species'] = df_grouped['Species'].replace(['Smelt', 'Parkki', 'Whitefish'], 'Others')
    df_grouped = df_grouped.drop(['Length2', 'Length3'], axis=1)

    Y = df_grouped['Species'].to_numpy()
    df_grouped = df_grouped.drop(['Species'], axis=1)

    scaler = MinMaxScaler()
    df_grouped_normalized = pd.DataFrame(scaler.fit_transform(df_grouped), columns=df_grouped.columns)
    
    return df_grouped_normalized, Y

def train_logistic_regression(X_train, y_train):
    logreg = LogisticRegression(solver='saga')
    logreg.fit(X_train, y_train)
    return logreg

def evaluate_classification_model(model, X_test, y_test):
    y_pred = model.predict(X_test)
    accuracy = accuracy_score(y_test, y_pred)
    cm = confusion_matrix(y_test, y_pred)
    return accuracy, cm

def plot_confusion_matrix(cm):
    plt.figure()
    sns.heatmap(cm, annot=True, cmap='Blues', fmt='d')
    plt.title("Confusion Matrix")
    plt.xlabel("Predicted")
    plt.ylabel("Actual")
    return plt

def train_softmax_regression(X_train, y_train):
    softmax = LogisticRegression(solver='lbfgs', max_iter=1000)
    params = {'C': [0.01, 0.1, 1, 10, 100]}
    softmax_grid = GridSearchCV(softmax, params, scoring='accuracy', cv=5)
    softmax_grid.fit(X_train, y_train)
    return softmax_grid.best_estimator_

def train_naive_bayes(X_train, y_train):
    nb = GaussianNB()
    nb.fit(X_train, y_train)
    return nb

def train_knn(X_train, y_train, n_neighbors=5):
    knn = KNeighborsClassifier(n_neighbors=n_neighbors)
    knn.fit(X_train, y_train)
    return knn

def plot_learning_curve(model, X_train, y_train):
    train_sizes, train_scores, test_scores = learning_curve(model, X_train, y_train, cv=5, scoring='accuracy', train_sizes=np.linspace(0.1, 1.0, 10))

    train_scores_mean = np.mean(train_scores, axis=1)
    test_scores_mean = np.mean(test_scores, axis=1)

    plt.figure()
    plt.plot(train_sizes, train_scores_mean, label='Training accuracy')
    plt.plot(train_sizes, test_scores_mean, label='Validation accuracy')
    plt.ylabel('Accuracy')
    plt.xlabel('Training set size')
    plt.title('Learning curves')
    plt.legend()
    plt.minorticks_on()
    plt.grid(which='both', linestyle='--', linewidth=0.5)
    return plt
