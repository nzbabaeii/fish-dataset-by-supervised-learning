# app.py

from sklearn import metrics
import streamlit as st
import matplotlib.pyplot as plt
import seaborn as sns
import ml  
from sklearn.metrics import classification_report, confusion_matrix, accuracy_score
from sklearn.model_selection import train_test_split

# Cache the data loading function
@st.cache_data
def load_data():
    return ml.load_data('Fish.csv')

# Set the title of the app
st.title("Fish Market Data Analysis")

# Sidebar for user interaction
st.sidebar.header('User Input Features')

# Load the data
df = load_data()

# Get all features for selection, including 'Weight'
all_features = df.columns.tolist()

# Sidebar for selecting features
selected_features = st.sidebar.multiselect(
    'Select Features to Use in Model:',
    all_features,
    default=all_features
)

st.sidebar.write("You selected these features:")
st.sidebar.write(selected_features)

# Toggle for visualization
visualize = st.sidebar.checkbox('Show Visualizations', value=True)

# Display the dataframe
st.write("### Fish Dataset")
st.write(df.head())

# Preprocessing
df_combined = ml.preprocess_data(df, selected_features)

# Add the target back to df_combined for splitting
if 'Weight' not in df_combined.columns:
    df_combined['Weight'] = df['Weight']

# Regression Section
st.sidebar.markdown('# The target is the Weight of fishes')
st.sidebar.header('Regression')
if st.sidebar.checkbox('Run Regression', value=False):
    # Correlation matrix
    if visualize:
        correlation_matrix = ml.correlation_matrix(df_combined)
        st.write("### Correlation Matrix")
        plt.figure(figsize=(10, 10))
        sns.heatmap(correlation_matrix, cbar=True, square=True, fmt='.1f', annot=True, annot_kws={'size': 10}, cmap='Blues')
        plt.minorticks_on()
        plt.grid(which='both', linestyle='--', linewidth=0.5)
        st.pyplot(plt)

    # Feature selection
    st.write("### Feature Selection")
    features_to_remove = st.multiselect(
        'Select Features to Remove:',
        df_combined.columns.tolist(),
        default=[]
    )

    df_combined.drop(columns=features_to_remove, inplace=True)
    st.write("Remaining Features:")
    st.write(df_combined.columns.tolist())

    # Split data
    X_train, X_test, y_train, y_test = ml.split_data(df_combined)

    # Multiple Linear Regression
    mlr_model = ml.train_mlr(X_train, y_train)

    # Predictions
    r2, mae, mse, rmse, y_pred = ml.evaluate_model(mlr_model, X_test, y_test)

    # Model evaluation
    st.write("### Multiple Linear Regression Model Evaluation")
    st.write('R^2:', r2)
    st.write('MAE:', mae)
    st.write('MSE:', mse)
    st.write('RMSE:', rmse)

    # Plot Actual vs Predicted
    if visualize:
        st.write("### Actual vs Predicted Weight")
        actual_vs_predicted_plot = ml.plot_actual_vs_predicted(y_test, y_pred)
        st.pyplot(actual_vs_predicted_plot)

    # Polynomial Regression
    degree = st.sidebar.slider('Select Degree for Polynomial Regression', min_value=2, max_value=5, value=2, step=1)

    poly_model, poly_features = ml.train_polynomial_regression(X_train, y_train, degree)

    X_test_poly = poly_features.transform(X_test)
    y_test_pred_poly = poly_model.predict(X_test_poly)

    test_mse_poly = metrics.mean_squared_error(y_test, y_test_pred_poly)
    test_r2_poly = metrics.r2_score(y_test, y_test_pred_poly)

    st.write(f"### Polynomial Regression (Degree {degree}) Model Evaluation")
    st.write(f'Test MSE: {test_mse_poly:.2f}')
    st.write(f'Test R2: {test_r2_poly:.2f}')

    # Plot Polynomial Regression
    if visualize:
        plt.figure(figsize=(10, 5))
        plt.scatter(y_test, y_test_pred_poly, edgecolors=(0, 0, 0))
        plt.plot([y_test.min(), y_test.max()], [y_test.min(), y_test.max()], 'k--', lw=2)
        plt.xlabel('Actual')
        plt.ylabel('Predicted')
        plt.title(f'Polynomial Regression (Degree {degree})')
        plt.minorticks_on()
        plt.grid(which='both', linestyle='--', linewidth=0.5)
        st.pyplot(plt)

    # Residuals Plot
    if visualize:
        st.write("### Residuals Histogram")
        residuals_plot = ml.plot_residuals(y_test, y_test_pred_poly)
        st.pyplot(residuals_plot)

# Classification Section
st.sidebar.markdown('# The target is the Species of fishes')
st.sidebar.header('Classification')
if st.sidebar.checkbox('Run Classification', value=False):
    # Preprocess data for classification
    df_grouped_normalized, Y = ml.preprocess_classification_data(df)
    X_train_class, X_test_class, y_train_class, y_test_class = train_test_split(df_grouped_normalized, Y, test_size=0.2, random_state=5)

    # Train Logistic Regression
    logreg_model = ml.train_logistic_regression(X_train_class, y_train_class)

    # Evaluate Classification Model
    accuracy, cm = ml.evaluate_classification_model(logreg_model, X_test_class, y_test_class)

    # Display Classification Results
    st.write("### Logistic Regression Classification Results")
    st.write(f'Accuracy: {accuracy:.2f}')
    
    # Plot Confusion Matrix
    if visualize:
        st.write("### Confusion Matrix")
        confusion_matrix_plot = ml.plot_confusion_matrix(cm)
        st.pyplot(confusion_matrix_plot)

    # Train and evaluate Softmax Regression
    softmax_model = ml.train_softmax_regression(X_train_class, y_train_class)
    y_pred_softmax = softmax_model.predict(X_test_class)
    st.write("### Softmax Regression Classification Results")
    st.write(f"Accuracy: {accuracy_score(y_test_class, y_pred_softmax):.2f}")
    st.write(classification_report(y_test_class, y_pred_softmax, zero_division=1))
    
    # Plot Confusion Matrix for Softmax Regression
    if visualize:
        st.write("### Confusion Matrix for Softmax Regression")
        cm_softmax = confusion_matrix(y_test_class, y_pred_softmax)
        confusion_matrix_plot_softmax = ml.plot_confusion_matrix(cm_softmax)
        st.pyplot(confusion_matrix_plot_softmax)

    # Train and evaluate Naive Bayes
    nb_model = ml.train_naive_bayes(X_train_class, y_train_class)
    y_pred_nb = nb_model.predict(X_test_class)
    st.write("### Naive Bayes Classification Results")
    st.write(f"Accuracy: {accuracy_score(y_test_class, y_pred_nb):.2f}")
    st.write(classification_report(y_test_class, y_pred_nb, zero_division=1))
    
    # Plot Confusion Matrix for Naive Bayes
    if visualize:
        st.write("### Confusion Matrix for Naive Bayes")
        cm_nb = confusion_matrix(y_test_class, y_pred_nb)
        confusion_matrix_plot_nb = ml.plot_confusion_matrix(cm_nb)
        st.pyplot(confusion_matrix_plot_nb)

    # Train and evaluate KNN
    n_neighbors = st.sidebar.slider('Select Number of Neighbors for KNN', min_value=1, max_value=20, value=5, step=1)
    knn_model = ml.train_knn(X_train_class, y_train_class, n_neighbors)
    y_pred_knn = knn_model.predict(X_test_class)
    st.write(f"### KNN (k={n_neighbors}) Classification Results")
    st.write(f"Accuracy: {accuracy_score(y_test_class, y_pred_knn):.2f}")
    st.write(classification_report(y_test_class, y_pred_knn, zero_division=1))
    
    # Plot Confusion Matrix for KNN
    if visualize:
        st.write("### Confusion Matrix for KNN")
        cm_knn = confusion_matrix(y_test_class, y_pred_knn)
        confusion_matrix_plot_knn = ml.plot_confusion_matrix(cm_knn)
        st.pyplot(confusion_matrix_plot_knn)
    
    # Learning Curve for KNN
    if visualize:
        st.write("### Learning Curve for KNN")
        learning_curve_plot = ml.plot_learning_curve(knn_model, X_train_class, y_train_class)
        st.pyplot(learning_curve_plot)
