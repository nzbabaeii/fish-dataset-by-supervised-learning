# Fish Market Data Analysis

This project provides a Streamlit app for analyzing fish market data using both regression and classification techniques. The app includes various visualizations and interactive features for exploring the dataset.

## Prerequisites

- Docker must be installed on your system. You can download and install Docker from [here](https://docs.docker.com/get-docker/).

## Running the App with Docker

Follow these steps to build and run the Docker container for the Streamlit app:

1. **Clone the Repository**: If you haven't already, clone this repository to your local machine.
    ```sh
    git clone <repository-url>
    cd <repository-directory>
    ```

2. **Build the Docker Image**: Build the Docker image using the provided `Dockerfile`.
    ```sh
    sudo docker build -t streamlit-app .
    ```

3. **Run the Docker Container**: Run the Docker container using the built image. If port 8501 is already in use, you can use a different port (e.g., 8502).
    ```sh
    ( docker run -p 8502:8501 streamlit-app)
    # If port 8501 is available
    sudo docker run -p 8501:8501 streamlit-app

    # If port 8501 is in use, use a different port (e.g., 8502)
    sudo docker run -p 8502:8501 streamlit-app
    ```

4. **Access the App**: Open your web browser and go to the following URL to access the app:
    - If using port 8501: [http://localhost:8501](http://localhost:8501)
    - If using port 8502: [http://localhost:8502](http://localhost:8502)

## Stopping the Docker Container

To stop the Docker container, you can use the `docker ps` command to list running containers and `docker stop` to stop the container.

1. **List Running Containers**:
    ```sh
    sudo docker ps
    ```

2. **Stop a Running Container**: Replace `CONTAINER_ID` with the actual container ID from the output of the previous command.
    ```sh
    sudo docker stop CONTAINER_ID
    ```

## Project Structure

- `app.py`: The main Streamlit app script.
- `ml.py`: Contains the functions for data preprocessing, model training, and evaluation.
- `requirements.txt`: Lists the Python dependencies required for the project.
- `Dockerfile`: The Dockerfile for building the Docker image.
- `Fish.csv`: The dataset file used in the analysis.

## Notes

- Ensure you have Docker installed and running on your system before following the above steps.
- If you encounter any issues with port allocation, try using a differ
